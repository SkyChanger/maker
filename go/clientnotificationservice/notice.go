// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package clientnotificationservice

import "sync"

type Level string

const LevelInfo = "info"
const LevelWarning = "warning"
const LevelError = "error"

type Notice struct {
	Level   Level                  `json:"level"`
	Message string                 `json:"message"`
	Data    map[string]interface{} `json:"data"`
}

func NewNotice(level Level, msg string) *Notice {
	return &Notice{
		Level:   level,
		Message: msg,
	}
}

func (n *Notice) WithData(data map[string]interface{}) *Notice {
	n.Data = data
	return n
}

type Service struct {
	lock        sync.RWMutex
	subscribers map[chan *Notice]bool
}

func New() *Service {
	return &Service{
		subscribers: make(map[chan *Notice]bool),
	}
}

func (s *Service) Subscribe() chan *Notice {
	s.lock.Lock()
	defer s.lock.Unlock()
	channel := make(chan *Notice)
	s.subscribers[channel] = true
	return channel
}

func (s *Service) Unsubscribe(channel chan *Notice) {
	s.lock.Lock()
	defer s.lock.Unlock()
	if _, exists := s.subscribers[channel]; !exists {
		return
	}
	delete(s.subscribers, channel)
}

func (s *Service) Broadcast(notice *Notice) {
	s.lock.RLock()
	defer s.lock.RUnlock()
	for channel := range s.subscribers {
		channel <- notice
	}
}
