// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/crankykernel/maker/go/server"
)

var ServerCmd = &cobra.Command{
	Use: "server",
	Run: func(cmd *cobra.Command, args []string) {
		server.ServerFlags.DataDirectory = DefaultDataDirectory
		server.ServerMain()
	},
}

func init() {
	flags := ServerCmd.Flags()
	flags.Int16VarP(&server.ServerFlags.Port, "port", "p", 6045, "Port")
	flags.StringVar(&server.ServerFlags.Host, "host", "127.0.0.1", "Host to bind to")
	flags.BoolVar(&server.ServerFlags.NoLog, "nolog", false, "Disable logging to file")
	flags.BoolVar(&server.ServerFlags.OpenBrowser, "open", false, "Open browser")

	flags.BoolVar(&server.ServerFlags.TLS, "tls", false, "Enable TLS")
	flags.BoolVar(&server.ServerFlags.NoTLS, "no-tls", false, "Disable TLS")
	flags.MarkHidden("no-tls")

	flags.BoolVar(&server.ServerFlags.EnableAuth, "auth", false, "Enable authentication")
	flags.BoolVar(&server.ServerFlags.NoAuth, "no-auth", false, "Disable authentication")
	flags.MarkHidden("no-auth")

	flags.BoolVar(&server.ServerFlags.LetsEncrypt, "letsencrypt", false, "Enable Lets Encrypt")
	flags.StringVar(&server.ServerFlags.LeHostname, "letsencrypt-hostname", "", "Lets Encrypt hostname")
	flags.StringVar(&server.ServerFlags.LeHostname, "le-hostname", "", "Lets Encrypt hostname")
	flags.MarkHidden("le-hostname")

	flags.BoolVar(&server.ServerFlags.ItsAllMyFault, "its-all-my-fault", false, "Its all my fault")
	flags.MarkHidden("its-all-my-fault")

	viper.SetEnvPrefix("MAKER")

	rootCmd.AddCommand(ServerCmd)
}
