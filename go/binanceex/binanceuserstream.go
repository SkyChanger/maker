// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package binanceex

import (
	"encoding/json"
	"fmt"
	"github.com/crankykernel/binanceapi-go"
	"gitlab.com/crankykernel/maker/go/clientnotificationservice"
	"gitlab.com/crankykernel/maker/go/config"
	"gitlab.com/crankykernel/maker/go/healthservice"
	"gitlab.com/crankykernel/maker/go/log"
	"strings"
	"sync"
	"time"
)

type StreamEventType string

const (
	EventTypeExecutionReport     StreamEventType = "executionReport"
	EventTypeOutboundAccountInfo StreamEventType = "outboundAccountInfo"
)

type ListenKeyWrapper struct {
	lock      sync.Mutex
	listenKey string
}

func NewListenKeyWrapper() *ListenKeyWrapper {
	return &ListenKeyWrapper{}
}

func (k *ListenKeyWrapper) Set(listenKey string) {
	k.lock.Lock()
	defer k.lock.Unlock()
	k.listenKey = listenKey
}

func (k *ListenKeyWrapper) Get() string {
	k.lock.Lock()
	defer k.lock.Unlock()
	return k.listenKey
}

type UserStreamEvent struct {
	EventType           StreamEventType
	EventTime           time.Time
	OutboundAccountInfo binanceapi.StreamOutboundAccountInfo
	ExecutionReport     binanceapi.StreamExecutionReport
	Raw                 []byte
}

type BinanceUserDataStream struct {
	Subscribers         map[chan *UserStreamEvent]string
	lock                sync.RWMutex
	listenKey           *ListenKeyWrapper
	notificationService *clientnotificationservice.Service
	healthService       *healthservice.Service
}

func NewBinanceUserDataStream(notificationService *clientnotificationservice.Service,
	healthService *healthservice.Service) *BinanceUserDataStream {
	return &BinanceUserDataStream{
		Subscribers:         make(map[chan *UserStreamEvent]string),
		listenKey:           NewListenKeyWrapper(),
		notificationService: notificationService,
		healthService:       healthService,
	}
}

func (b *BinanceUserDataStream) Subscribe(name string) chan *UserStreamEvent {
	b.lock.Lock()
	defer b.lock.Unlock()
	channel := make(chan *UserStreamEvent, 3)
	b.Subscribers[channel] = name
	return channel
}

func (b *BinanceUserDataStream) Unsubscribe(channel chan *UserStreamEvent) {
	b.lock.Lock()
	defer b.lock.Unlock()
	delete(b.Subscribers, channel)
}

func (b *BinanceUserDataStream) ListenKeyRefreshLoop() {
	for {
		time.Sleep(time.Minute)
		listenKey := b.listenKey.Get()
		if listenKey == "" {
			log.Debugf("No Binance user stream key set, will not refresh")
		} else {
			log.Debugf("Refreshing Binance user stream listen key")
			client := GetBinanceRestClient()
			if err := client.PutUserStreamKeepAlive(listenKey); err != nil {
				log.WithError(err).Errorf("Failed to send Binance user stream keep alive.")
				b.notificationService.Broadcast(
					clientnotificationservice.NewNotice(
						clientnotificationservice.LevelError,
						fmt.Sprintf("Binance user socket error: %s", err.Error())).
						WithData(map[string]interface{}{
							"binanceUserSocketState": "failed",
						}))
			}
		}
	}
}

func (b *BinanceUserDataStream) getListenKey() (string, error) {
	configChannel := config.Subscribe()
	defer config.Unsubscribe(configChannel)
Retry:
	apiKey := config.GetString("binance.api.key")
	if apiKey == "" {
		log.Infof("Binance API key not set. Waiting for configuration update.")
		<-configChannel
		goto Retry
	}
	listenKey, err := GetBinanceRestClient().GetUserDataStream()
	return listenKey, err
}

func (b *BinanceUserDataStream) Run() {
	b.healthService.Update(healthservice.BINANCE_USER_SOCKET_STATE_CONNECTING)
	userErrorMessage := ""
	go b.ListenKeyRefreshLoop()
	goto Start

Fail:
	b.listenKey.Set("")
	b.notificationService.Broadcast(
		clientnotificationservice.NewNotice(
			clientnotificationservice.LevelError,
			fmt.Sprintf("Binance user socket error: %s", userErrorMessage)).
			WithData(map[string]interface{}{
				"binanceUserSocketState": "failed",
			}))
	b.healthService.Update(healthservice.BINANCE_USER_SOCKET_STATE_CONNECTION_FAILED)
	time.Sleep(time.Second * 6)

Start:
	// First we have to get the user stream listen key.
	listenKey, err := b.getListenKey()
	if err != nil {
		log.WithError(err).Error("Failed to get Binance user stream key. Retrying.")
		userErrorMessage = err.Error()
		goto Fail
	} else {
		log.WithFields(log.Fields{}).Debugf("Acquired Binance user stream listen key")
	}

	userStream, err := binanceapi.OpenSingleStream(listenKey)
	if err != nil {
		log.WithError(err).Errorf("Failed to open Binance user stream")
		userErrorMessage = err.Error()
		goto Fail
	}
	b.listenKey.Set(listenKey)

	log.Infof("Connected to Binance user stream websocket.")
	userStream.Conn.SetPongHandler(func(appData string) error {
		log.WithFields(log.Fields{
			"data": appData,
		}).Debugf("Received Binance user stream pong")
		return nil
	})

	b.notificationService.Broadcast(
		clientnotificationservice.NewNotice(
			clientnotificationservice.LevelInfo,
			"Connected to Binance user data stream.").WithData(map[string]interface{}{
			"binanceUserSocketState": "ok",
		}))

	b.healthService.Update(healthservice.BINANCE_USER_SOCKET_STATE_OK)

	for {
		message, err := userStream.Next()
		if err != nil {
			log.WithError(err).Errorf("Failed to read next Binance user stream message")
			userErrorMessage = err.Error()
			goto Fail
		}

		streamEvent := UserStreamEvent{}
		streamEvent.Raw = message

		switch {
		case strings.HasPrefix(string(message), `{"e":"executionReport",`):
			var orderUpdate binanceapi.StreamExecutionReport
			if err := json.Unmarshal(message, &orderUpdate); err != nil {
				log.WithError(err).Error("Failed to decode user stream executionReport message.")
				continue
			}
			streamEvent.EventType = StreamEventType(orderUpdate.EventType)
			streamEvent.EventTime = time.Unix(0, orderUpdate.EventTimeMillis*int64(time.Millisecond))
			streamEvent.ExecutionReport = orderUpdate
		case strings.HasPrefix(string(message), `{"e":"outboundAccountInfo",`):
			if err := json.Unmarshal(message, &streamEvent.OutboundAccountInfo); err != nil {
				log.WithError(err).Error("Failed to decode user stream outboundAccountInfo message.")
				continue
			}
			streamEvent.EventType = StreamEventType(streamEvent.OutboundAccountInfo.EventType)
			streamEvent.EventTime = time.Unix(0, streamEvent.OutboundAccountInfo.EventTimeMillis*int64(time.Millisecond))
		}

		b.lock.RLock()
		for channel := range b.Subscribers {
			select {
			case channel <- &streamEvent:
			}
		}
		b.lock.RUnlock()
	}
}
