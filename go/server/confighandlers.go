// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package server

import (
	"encoding/json"
	"github.com/crankykernel/binanceapi-go"
	"gitlab.com/crankykernel/maker/go/config"
	"gitlab.com/crankykernel/maker/go/log"
	"io/ioutil"
	"net/http"
)

func SavePreferencesHandler(w http.ResponseWriter, r *http.Request) {
	type preferenceConfig struct {
		BalancePercents        string  `json:"preferences.balance.percents"`
		BinanceBnbWarningLevel float64 `json:"preferences.binance.bnbwarninglevel"`
	}

	var request preferenceConfig
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.WithFields(log.Fields{
			"path":   r.URL.Path,
			"method": r.Method,
		}).WithError(err).Errorf("Failed to decode Binance configuration.")
		WriteJsonError(w, http.StatusBadRequest, err.Error())
		return
	}
	err = json.Unmarshal(body, &request)
	if err != nil {
		log.WithFields(log.Fields{
			"path":   r.URL.Path,
			"method": r.Method,
		}).WithError(err).Errorf("Failed to decode Binance configuration.")
		WriteJsonError(w, http.StatusBadRequest, err.Error())
		return
	}

	log.Printf("%+v", request)

	config.Set("preferences.balance.percents", request.BalancePercents)
	config.SetFloat64("preferences.binance.bnbwarninglevel", request.BinanceBnbWarningLevel)
	config.WriteConfig(ServerFlags.ConfigFilename)
}

func SaveBinanceConfigHandler(w http.ResponseWriter, r *http.Request) {
	type binanceApiConfiguration struct {
		ApiKey    string `json:"key"`
		ApiSecret string `json:"secret"`
	}

	var request binanceApiConfiguration
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&request); err != nil {
		log.WithFields(log.Fields{
			"path":   r.URL.Path,
			"method": r.Method,
		}).WithError(err).Errorf("Failed to decode Binance configuration.")
		WriteJsonError(w, http.StatusBadRequest, err.Error())
		return
	}

	config.Set("binance.api.key", request.ApiKey)
	config.Set("binance.api.secret", request.ApiSecret)
	config.WriteConfig(ServerFlags.ConfigFilename)
}

func BinanceTestHandler(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		WriteJsonError(w, http.StatusBadRequest, "failed to parse form data")
		return
	}

	binanceApiKey := r.FormValue("binance.api.key")
	if binanceApiKey == "" {
		WriteJsonError(w, http.StatusBadRequest, "missing binance.api.key")
		return
	}
	binanceApiSecret := r.FormValue("binance.api.secret")
	if binanceApiSecret == "" {
		WriteJsonError(w, http.StatusBadRequest, "missing binance.api.secret")
		return
	}

	client := binanceapi.NewRestClient().WithAuth(binanceApiKey, binanceApiSecret)
	_, err := client.GetAccount()
	if err != nil {
		log.WithError(err).Warn("Binance account authentication test failed.")
		WriteJsonResponse(w, http.StatusOK, map[string]interface{}{
			"ok":    false,
			"error": err.Error(),
		})
		return
	}

	WriteJsonResponse(w, http.StatusOK, map[string]interface{}{
		"ok": true,
	})
}
