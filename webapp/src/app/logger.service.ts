// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import {Injectable} from "@angular/core";

@Injectable({
    providedIn: "root",
})
export class LoggerService {

    constructor() {
    }

    log(msg) {
        console.log(msg);
    }

    logWithPrefix(prefix, msg) {
        switch (typeof msg) {
            case "string":
                this.log(`${prefix}: ${msg}`);
                break;
            default:
                this.log(`${prefix}: ${JSON.stringify(msg)}`);
                break;
        }
    }

    getLogger(prefix: string): Logger {
        return new Logger(prefix, this);
    }
}

export class Logger {
    constructor(private prefix: string, private logger: LoggerService) {
    }

    log(msg) {
        this.logger.logWithPrefix(this.prefix, msg);
    }
}
