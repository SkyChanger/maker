// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import {Injectable} from '@angular/core';
import {MakerApiService} from "./maker-api.service";
import {ReplaySubject, Subject} from "rxjs";
import {LoginService} from "./login.service";

export enum MakerSocketState {
    INITIALIZING = "initializing",
    CONNECTING = "connecting",
    CONNECTED = "connected",
    ERROR = "error",
    DISCONNECTED = "disconnected"
}

@Injectable({
    providedIn: 'root'
})
export class MakerSocketService {

    public state: MakerSocketState = MakerSocketState.INITIALIZING;

    private reconnects = 0;

    $messages = new Subject();

    stateChange$: ReplaySubject<string> = new ReplaySubject();

    constructor(private makerApi: MakerApiService,
                private loginService: LoginService) {
        this.stateChange$.next(this.state);
    }

    start() {
        this.startConnect();
    }

    private onMessage(msg: any) {
        this.$messages.next(msg);
    }

    private log(msg: string) {
        console.log("maker-socket: " + msg);
    }

    private setState(state: MakerSocketState) {
        this.state = state;
        this.stateChange$.next(this.state);
    }

    private startConnect() {
        this.makerApi.getVersion().subscribe((response) => {
            this.log("Version check OK, procedding with websocket connection.");
            this.connect();
        }, (error) => {
            if (error.status === 401) {
                this.loginService.gotoLogin();
                return;
            } else {
                setTimeout(() => {
                    this.startConnect();
                }, 1000);
            }
        });
    }

    private connect() {
        this.state = MakerSocketState.CONNECTING;

        const ws = this.makerApi.openWebsocket();

        ws.onopen = () => {
            this.setState(MakerSocketState.CONNECTED);
            this.reconnects = 0;
            this.log("connected");
        };

        ws.onerror = () => {
            this.setState(MakerSocketState.ERROR);
            this.log("an error occurred, disconnecting");
            ws.close();
        };

        ws.onmessage = (event) => {
            try {
                const message = JSON.parse(event.data);
                this.onMessage(message);
            } catch (err) {
                this.log("error: failed to parse message:");
                console.log(event);
            }
        };

        ws.onclose = () => {
            if (this.state !== MakerSocketState.ERROR) {
                this.setState(MakerSocketState.DISCONNECTED);
            }
            this.log("closed");
            this.startConnect();
        };
    }

}
