// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import {Injectable} from "@angular/core";

import * as toastr from "toastr";

export interface ToastrOptions {
    progressBar?: boolean;
    timeOut?: number;
    extendedTimeOut?: number;
    closeButton?: boolean;
    preventDuplicates?: boolean;
    preventOpenDuplicates?: boolean;
    onHidden?: any;
}

@Injectable()
export class ToastrService {

    constructor() {
        toastr.options.toastClass = "toastr";
    }

    success(msg: string, title?: string, options?: ToastrOptions) {
        toastr.success(msg, title, options);
    }

    info(msg: string, title?: string, options?: ToastrOptions) {
        toastr.info(msg, title, options);
    }

    warning(msg: string, title?: string, options?: ToastrOptions) {
        toastr.warning(msg, title, options);
    }

    error(msg: string, title?: string, options?: ToastrOptions) {
        toastr.error(msg, title, options);
    }
}
