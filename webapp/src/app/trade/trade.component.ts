// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from "@angular/core";
import {BinanceAccountInfo, BinanceApiService, BinanceBalance} from "../binance-api.service";
import {Observable} from "rxjs";
import {BinanceService, LimitSellType, OpenTradeOptions, PriceSource} from "../binance.service";
import {switchMap, tap} from "rxjs/operators";
import {Subscription} from "rxjs/Subscription";
import * as Mousetrap from "mousetrap";
import * as $ from "jquery";
import {round8, roundx} from "../utils";
import {ConfigKey, ConfigService, DEFAULT_BINANCE_BNB_WARNING_LEVEL} from "../config.service";
import {MakerService, TradeMap} from "../maker.service";
import {Logger, LoggerService} from "../logger.service";
import {FormBuilder, FormGroup} from "@angular/forms";
import {ToastrService} from "../toastr.service";
import {TradeTableComponent} from '../trade-table/trade-table.component';
import {ActivatedRoute, Router} from '@angular/router';
import {BinanceProxyService} from "../binance-proxy.service";

declare var window: any;

/**
 * The interface for the parts of the order form that should be saved between
 * trades and reloads.
 */
interface OrderFormSettingsModel {
    quoteAsset: string;
    symbol: string;
    priceSource: PriceSource;
    balancePercent: number;
    stopLossEnabled: boolean;
    stopLossPercent: number;
    trailingProfitEnabled: boolean;
    trailingProfitPercent: number;
    trailingProfitDeviation: number;
    limitSellEnabled: boolean;
    limitSellPercent: number;
    limitSellPriceEnabled: boolean;
}

interface SavedState {
    orderFormSettings: OrderFormSettingsModel;
}

@Component({
    templateUrl: "./trade.component.html",
    styleUrls: ["./trade.component.scss"]
})
export class TradeComponent implements OnInit, OnDestroy, AfterViewInit {

    OFFSET_TYPE_NONE = "NONE";
    OFFSET_TYPE_TICKS = "TICKS";

    private localStorageKey = "binance.trade.component";

    BuyPriceSource = PriceSource;

    ticker: {
        last?: number,
        bid?: number,
        ask?: number,
        vol24?: number,
        percentChange24?: number;
    } = {};

    orderFormSettings: OrderFormSettingsModel = {
        quoteAsset: "BTC",
        symbol: "ETHBTC",
        priceSource: PriceSource.BEST_BID,
        balancePercent: null,
        stopLossEnabled: false,
        stopLossPercent: 1,
        trailingProfitEnabled: false,
        trailingProfitPercent: 1,
        trailingProfitDeviation: 0.25,
        limitSellEnabled: false,
        limitSellPercent: 0.1,
        limitSellPriceEnabled: false,
    };

    // Parts of the order form that don't persist.
    orderForm: {
        amount: number;
        quoteAmount: number;
        buyLimitPercent: number;
        manualPrice: string;
        limitSellPrice: string;

        offsetEnabled: boolean;
        offsetTicks: number;
    } = {
        amount: null,
        quoteAmount: null,
        buyLimitPercent: null,
        manualPrice: null,
        limitSellPrice: null,

        offsetEnabled: false,
        offsetTicks: 0,
    };

    balances: { [key: string]: BinanceBalance } = {};

    private tickerSubscription: Subscription = null;

    balancePercents: number[] = [];

    private subs: Subscription[] = [];

    private logger: Logger = null;

    trailingProfitForm: FormGroup;

    priceStepSize: number = 0.00000001;

    viewTrades = "all";

    bnbWarningLevel: number = DEFAULT_BINANCE_BNB_WARNING_LEVEL;

    @ViewChild(TradeTableComponent, {static: false}) private tradeTable: TradeTableComponent;

    constructor(private api: BinanceApiService,
                public binance: BinanceService,
                private binanceApi: BinanceApiService,
                private maker: MakerService,
                private config: ConfigService,
                private formBuilder: FormBuilder,
                private toastr: ToastrService,
                private route: ActivatedRoute,
                private router: Router,
                logger: LoggerService,
                private binanceProxy: BinanceProxyService,
    ) {
        this.logger = logger.getLogger("trade-component");
    }

    private resetTicker() {
        this.ticker = {
            last: 0,
            bid: 0,
            ask: 0,
        };
    }

    ngOnInit() {
        this.resetTicker();
        this.config.loadConfig().subscribe(() => {
            this.balancePercents = this.config.getBalancePercents();
            this.orderFormSettings.balancePercent = this.balancePercents[0];
            this.bnbWarningLevel = this.config.config[ConfigKey.PREFERENCE_BINANCE_BNB_WARNING_LEVEL];
            this.reloadState();

            // Setup the trailing stop reactive form. It might be more work
            // than its worth for the reactive vs template form, at least for
            // this use case.
            this.trailingProfitForm = this.formBuilder.group({
                enabled: [{
                    value: this.orderFormSettings.trailingProfitEnabled,
                    disabled: false,
                }],
                percent: [{
                    value: this.orderFormSettings.trailingProfitPercent,
                    disabled: !this.orderFormSettings.trailingProfitEnabled,
                }],
                deviation: [{
                    value: this.orderFormSettings.trailingProfitDeviation,
                    disabled: !this.orderFormSettings.trailingProfitEnabled,
                }],
            });
            let s = this.trailingProfitForm.valueChanges.subscribe((data) => {
                this.orderFormSettings.trailingProfitEnabled = data.enabled;
                if (data.percent != undefined) {
                    this.orderFormSettings.trailingProfitPercent = data.percent;
                }
                if (data.percent != undefined) {
                    this.orderFormSettings.trailingProfitDeviation = data.deviation;
                }
                if (data.enabled) {
                    this.trailingProfitForm.controls.percent.enable({emitEvent: false});
                    this.trailingProfitForm.controls.deviation.enable({emitEvent: false});
                } else {
                    this.trailingProfitForm.controls.percent.disable({emitEvent: false});
                    this.trailingProfitForm.controls.deviation.disable({emitEvent: false});
                }
                this.saveState();
            });
            this.subs.push(s);
        });

        this.binance.isReady$.pipe(switchMap(() => {
            return this.updateAccountInfo();
        })).subscribe(() => {
            this.changeSymbol();
        }, (error) => {
            // Error.
            console.log("Failed to udpate account info:");
            console.log(error);
            this.toastr.error("Failed to update account information, see browser console for more details.");
        }, () => {
        });

        if (this.route.snapshot.params.symbol) {
            this.orderFormSettings.symbol = this.route.snapshot.params.symbol;
            console.log(`Initialized symbol to ${this.orderFormSettings.symbol}`);
        }

        this.binance.isReady$.subscribe(() => {
            let s = this.route.queryParams.subscribe((params) => {
                const newSymbol = params.symbol;
                if (newSymbol && newSymbol != this.orderFormSettings.symbol) {
                    this.changeSymbol(newSymbol);
                }

                switch (params.viewTrades) {
                    case "open":
                        this.viewTrades = "open";
                        break;
                    case "closed":
                        this.viewTrades = "closed";
                        break;
                    default:
                        this.viewTrades = "all";
                        break;
                }
            });
            this.subs.push(s);
        });

        let s = this.maker.binanceAccountInfo$.subscribe((accountInfo) => {
            this.updateBalances(accountInfo);
        });
        this.subs.push(s);

        Mousetrap.bind("/", () => {
            window.scrollTo(0, 0);
            $("#symbolInput").focus();
        });

    }

    ngAfterViewInit() {
        this.binance.isReady$.subscribe(() => {
            let s = this.maker.tradeMap$.subscribe((tradeMap: TradeMap) => {
                setTimeout(() => {
                    this.tradeTable.onTradeMapUpdate(tradeMap);
                }, 0);
            });
            this.subs.push(s);

            s = this.maker.binanceAggTrades$.subscribe((aggTrade) => {
                setTimeout(() => {
                    this.tradeTable.onAggTrade(aggTrade);
                }, 0);
            });
            this.subs.push(s);
        });
    }

    ngOnDestroy() {
        if (this.tickerSubscription) {
            this.tickerSubscription.unsubscribe();
        }
        for (const sub of this.subs) {
            sub.unsubscribe();
        }
    }

    private updateAccountInfo(): Observable<BinanceAccountInfo> {
        return this.binanceProxy.getAccountInfo().pipe(tap((accountInfo) => {
            console.log("Updating account info.");
            this.updateBalances(accountInfo);
        }));
    }

    private saveState() {
        console.log("Saving state.");
        const state: SavedState = {
            orderFormSettings: this.orderFormSettings,
        };
        localStorage.setItem(this.localStorageKey, JSON.stringify(state));
    }

    private reloadState() {
        const rawState = localStorage.getItem(this.localStorageKey);
        if (!rawState) {
            this.logger.log("No saved state in local storage.");
            return;
        }
        try {
            const savedState: SavedState = JSON.parse(rawState);
            if (savedState.orderFormSettings) {
                Object.assign(this.orderFormSettings, savedState.orderFormSettings);
            }
        } catch (err) {
            console.log("error: failed to restore saved status:");
            console.log(err);
        }

        if (this.orderFormSettings.priceSource == PriceSource.MANUAL) {
            this.orderForm.offsetEnabled = false;
        } else {
            this.orderForm.offsetEnabled = true;
        }
    }

    private updateBalances(accountInfo: BinanceAccountInfo) {
        for (const balance of accountInfo.balances) {
            this.balances[balance.asset] = balance;
        }
    }

    changeQuoteAsset() {
        this.binance.updateExchangeInfo().subscribe();
        this.saveState();
    }

    changeSymbol(symbol: string = null) {

        this.resetTicker();

        if (symbol != null) {
            this.orderFormSettings.symbol = symbol;
        } else {
            symbol = this.orderFormSettings.symbol;
        }

        this.router.navigate(["/trade"], {
            queryParams: {
                symbol: symbol,
            },
            queryParamsHandling: "merge",
        });

        if (!symbol) {
            this.saveState();
            return;
        }

        this.binanceProxy.getTicker24h(symbol).subscribe((response) => {
            this.ticker.last = +response.lastPrice;
            this.ticker.percentChange24 = +response.priceChangePercent;
            this.ticker.vol24 = +response.quoteVolume;
            this.ticker.bid = +response.bidPrice;
            this.ticker.ask = +response.askPrice;

            this.orderForm.manualPrice = this.ticker.last.toFixed(8);
            this.orderForm.limitSellPrice = this.ticker.last.toFixed(8);
            this.updateOrderFormAssetAmount();
        });

        this.priceStepSize = this.binance.symbolMap[symbol].tickSize;

        if (this.tickerSubscription) {
            this.tickerSubscription.unsubscribe();
        }

        this.tickerSubscription = this.binance.subscribeToTicker(symbol).subscribe((ticker) => {
            this.ticker.bid = ticker.bestBid;
            this.ticker.ask = ticker.bestAsk;
            this.ticker.vol24 = ticker.volume;
            this.ticker.percentChange24 = ticker.percentChange24;
            this.ticker.last = ticker.price;
            this.updateOrderFormAssetAmount();
        });

        this.saveState();
    }

    syncManualPrice() {
        this.orderForm.manualPrice = this.ticker.last.toFixed(8);
        this.updateOrderFormAssetAmount();
    }

    fixManualPriceInput() {
        this.orderForm.manualPrice = this.toFixed(+this.orderForm.manualPrice, 8);
        this.updateOrderFormAssetAmount();
    }

    updateOrderFormAssetAmount() {
        if (!this.balances[this.orderFormSettings.quoteAsset]) {
            return;
        }
        const symbol = this.orderFormSettings.symbol;
        const symbolInfo = this.binance.symbolMap[symbol];
        const stepSize = symbolInfo.stepSize;
        const available = this.balances[this.orderFormSettings.quoteAsset].free * 0.999;
        const portion = round8(available * this.orderFormSettings.balancePercent / 100);
        switch (this.orderFormSettings.priceSource) {
            case PriceSource.MANUAL:
                const amount = roundx(portion / +this.orderForm.manualPrice, 1 / stepSize);
                this.orderForm.amount = amount;
                break;
            default:
                break;
        }
        const lastTradePrice = this.ticker.last;
        const amount = roundx(portion / lastTradePrice, 1 / stepSize);
        this.orderForm.quoteAmount = portion;
        this.orderForm.amount = amount;
    }

    makeOrder() {
        const options: OpenTradeOptions = {
            symbol: this.orderFormSettings.symbol,
            quantity: this.orderForm.amount,
            priceSource: this.orderFormSettings.priceSource,
            priceAdjustment: this.orderForm.buyLimitPercent,
            stopLossEnabled: this.orderFormSettings.stopLossEnabled,
            stopLossPercent: this.orderFormSettings.stopLossPercent,
            trailingProfitEnabled: this.orderFormSettings.trailingProfitEnabled,
            trailingProfitPercent: this.orderFormSettings.trailingProfitPercent,
            trailingProfitDeviation: this.orderFormSettings.trailingProfitDeviation,
            price: +this.orderForm.manualPrice,

            offsetTicks: 0,
        };

        if (this.orderFormSettings.priceSource != PriceSource.MANUAL) {
            options.offsetTicks = +this.orderForm.offsetTicks;
        }

        if (this.orderFormSettings.limitSellEnabled) {
            options.limitSellEnabled = true;
            options.limitSellType = LimitSellType.PERCENT;
            options.limitSellPercent = this.orderFormSettings.limitSellPercent;
        } else if (this.orderFormSettings.limitSellPriceEnabled) {
            options.limitSellEnabled = true;
            options.limitSellType = LimitSellType.PRICE;
            options.limitSellPrice = +this.orderForm.limitSellPrice;
        }

        this.maker.postBuyOrder(options).subscribe(() => {
        }, (error) => {
            console.log("Failed to post order:");
            console.log(error);
            const title = "Failed to Post Order";
            const options = {
                closeButton: true,
            };
            if (error.error) {
                console.log(`Failed to post order: ${JSON.stringify(error.error)}`);
                const inner = error.error;
                if (inner.code && inner.msg) {
                    this.toastr.error(`[${inner.code}]: ${inner.msg}`, title, options)
                } else {
                    this.toastr.error(JSON.stringify(inner), title, options);
                }
                return;
            } else if (error.message) {
                this.toastr.error(`${error.message}`, title, options);
            } else {
                this.toastr.error("Unknown error. Check server log and browser console.", title, options);
            }
        });
    }

    toggleLimitSellType(type: string) {
        if (type == 'PERCENT') {
            if (this.orderFormSettings.limitSellPriceEnabled) {
                this.orderFormSettings.limitSellPriceEnabled = false;
            }
            this.orderFormSettings.limitSellEnabled = !this.orderFormSettings.limitSellEnabled;
        } else if (type == 'PRICE') {
            this.orderFormSettings.limitSellEnabled = false;
            this.orderFormSettings.limitSellPriceEnabled = !this.orderFormSettings.limitSellPriceEnabled;
        }

        if (this.orderFormSettings.limitSellPriceEnabled) {
            this.orderForm.limitSellPrice = this.ticker.last.toFixed(8);
        }

    }

    onPriceSourceChange() {
        if (this.orderFormSettings.priceSource == PriceSource.MANUAL) {
            this.orderForm.offsetEnabled = false;
        } else {
            this.orderForm.offsetEnabled = true;
        }
        console.log(this.orderForm.offsetEnabled);
        this.updateOrderFormAssetAmount();
    }

    toFixed(value: number | string, fractionDigits: number): string {
        return (+value).toFixed(fractionDigits);
    }
}
