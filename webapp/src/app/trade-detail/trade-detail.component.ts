// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {MakerService, TradeState} from '../maker.service';
import {AppTradeState, toAppTradeState} from '../trade-table/trade-table.component';
import {BinanceService} from '../binance.service';
import {Trade} from '../trade';
import {MakerApiService} from "../maker-api.service";

@Component({
    selector: 'app-trade-detail',
    templateUrl: './trade-detail.component.html',
    styleUrls: ['./trade-detail.component.scss']
})
export class TradeDetailComponent implements OnInit, OnDestroy {

    private subs: Subscription[] = [];

    trade: AppTradeState = null;

    constructor(private route: ActivatedRoute,
                private binance: BinanceService,
                private makerApi: MakerApiService,
                private maker: MakerService) {
    }

    ngOnInit() {
        let s = this.route.params.subscribe((params) => {
            this.makerApi.get(`/api/trade/${params.tradeId}`).subscribe((trade: TradeState) => {
                this.trade = toAppTradeState(trade);
            })
        });
        this.subs.push(s);

        s = this.maker.trade$.subscribe((trade) => {
            if (this.trade && this.trade.TradeID == trade.TradeID) {
                this.trade = toAppTradeState(trade);
            }
        });
        this.subs.push(s);

        s = this.maker.binanceAggTrades$.subscribe((trade) => {
            if (this.trade && Trade.isOpen(this.trade) &&
                this.trade.Symbol == trade.symbol) {
                Trade.onLastPrice(this.trade, trade.price);
            }
        });
        this.subs.push(s);

    }

    ngOnDestroy() {
        for (const sub of this.subs) {
            sub.unsubscribe();
        }
    }
}
