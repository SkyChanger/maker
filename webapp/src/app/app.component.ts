// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import {Component, OnInit} from '@angular/core';
import {BinanceService} from './binance.service';
import {LoginService} from "./login.service";
import {MakerApiService} from "./maker-api.service";
import {MakerSocketService, MakerSocketState} from "./maker-socket.service";
import {MakerService} from "./maker.service";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    ticker: { [key: string]: number } = {};

    alertClass: string = "alert-dark";

    status: any = {
        makerSocketOk: false,
        makerSocketState: "initializing",
        binanceUserSocketOk: false,
        binanceUserSocketState: "initializing",
    };

    constructor(private binance: BinanceService,
                public makerApi: MakerApiService,
                public loginService: LoginService,
                private makerService: MakerService,
                private makerSocket: MakerSocketService) {
        this.status.makerSocketState = makerSocket.state;
    }

    ngOnInit() {
        this.binance.isReady$.subscribe(() => {
            this.binance.subscribeToTicker("BTCUSDT").subscribe((ticker) => {
                this.ticker[ticker.symbol] = ticker.price;
            });
        });

        this.makerSocket.stateChange$.subscribe((state: string) => {
            console.log("Make socket state changed: " + state);
            if (state === MakerSocketState.CONNECTED) {
                this.status.makerSocketOk = true;
                this.status.makerSocketState = "OK";
            } else {
                this.status.makerSocketOk = false;
                this.status.makerSocketState = state;

                this.status.binanceUserSocketState = "unknown";
                this.status.binanceUserSocketOk = false;
            }
            this.updateAlertColor();
        });

        this.makerService.statusUpdate$.subscribe((status) => {
            console.log("Maker server status: " + JSON.stringify(status));
            if (status.binanceUserSocketState === "ok") {
                this.status.binanceUserSocketOk = true;
                this.status.binanceUserSocketState = "OK"
            } else {
                this.status.binanceUserSocketOk = false;
                this.status.binanceUserSocketState = status.binanceUserSocketState || "unknown";
            }
            this.updateAlertColor();
        });

    }

    private updateAlertColor() {
        if (this.status.makerSocketOk && this.status.binanceUserSocketOk) {
            this.alertClass = "alert-success";
        } else {
            this.alertClass = "alert-danger";
        }
    }

    logout() {
        this.loginService.logout();
    }
}
